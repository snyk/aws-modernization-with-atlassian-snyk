+++
title = "Getting Started"
chapter = true
weight = 20
+++

# Getting Started

![Keys](../images/keys.png)

{{% notice tip %}}
You will need __both__ a Snyk and Atlassian account to proceed with the exercises.
{{% /notice %}}
